<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="icon" href="img/favicon.ico">
	<link rel="stylesheet/less" type="text/css" href="css/style.less">
	<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.7.2/less.min.js"></script>
	<script src="https://use.fontawesome.com/6b728270e5.js"></script>
	<title>JINESI YELIZATI's CV</title>
</head>
<body>
	<div class="wrap">
		<div class="left">
			<div class="photo">
				<img src="img/me.jpg">
			</div>

			<table cellspacing="0" cellpadding="0">
				<thead>
					<tr><th colspan="2"><H3>A</H3><H4>BOUT </H4><H3>M</H3><H4>E</H4></th></tr>
				</thead>
				<tbody>
					<tr>
						<th>Name：</th>
						<td>Jinesi Yelizati</td>
					</tr>
					<tr>
						<th>Nation：</th>
						<td>Chinese</td>
					</tr>
					<tr>
						<th>Gender：</th>
						<td>Male</td>
					</tr>
					<tr>
						<th>Born：</th>
						<td>08 June 1995</td>
					</tr>
					<tr>
						<th>Height：</th>
						<td>175cm</td>
					</tr>
					<tr>
						<th>Marital Status：</th>
						<td>Unmarried</td>
					</tr>
				</tbody>	
			</table>
			<table cellspacing="0" cellpadding="0">
				<thead>
					<tr><th colspan="2"><H3>C</H3><H4>ONTACT </H4><H3>M</H3><H4>E</H4></th></tr>
				</thead>
				<tbody>
					<tr>
						<th><i class="fa fa-map-marker fa-lg" aria-hidden="true" > &nbsp &nbsp </i>  </th>
						<td>Bristol, U.K.</td>
					</tr>
					<tr>
						<th><i class="fa fa-phone-square fa-lg" aria-hidden="true"> &nbsp &nbsp</i>  </th>
						<td>(+44)7802776003</td>
					</tr>
					<tr>
						<th><i class="fa fa-envelope-square fa-lg" aria-hidden="true"> &nbsp &nbsp </i> </th>
						<td><a href="mailto:Elzat.awar@gmail.com?subject=Mail from CV">Elzat.awar@gmail.com</a></td>
					</tr>
					<tr>
						<th><i class="fa fa-linkedin-square fa-lg" aria-hidden="true"> &nbsp &nbsp </i> </th>
						<td><a href="https://www.linkedin.com/in/ylzat-jines-21b7b3b1/">linkedin.com/in/ylzat-jines-21b7b3b1/</a></td>
					</tr>
				</tbody>	
			</table>

			<table cellspacing="0" cellpadding="0">
				<thead>
					<tr><th colspan="2"><H3>L</H3><H4>ANGUAGES </H4></th></tr>
				</thead>
				<tbody>
					<tr>
						<td><img src="img/united-kingdom.png"></td>
						<td>&nbsp <h4>English - </h4> IELTS: 6.5 </td>
					</tr>				
					<tr>
						<td><img src="img/china.png"></td>
						<td> &nbsp <h4>Chinese - </h4> Native  </td>
					</tr>
					<tr>
						<td><img src="img/kazakhstan.png"></td>
						<td> &nbsp <h4>Kazakh - </h4> Native </td>
					</tr>
				</tbody>	
			</table>
		</div>

		<div class="right">
			<div class="rhead">
				<div class="text">
					<h1>Jinesi Yelizati</h1>
					<h2>Engineering Intern</h2>
					<h3>Ylzat@2017</h3>
				</div>
			</div>
			<div class="profile">
				<p>
					PGT student in optical communications filed with practical experience in fiber laser systems and web developing. Seeking an internship opportunity in Engineering Program.
				</p>
				<div class="detail">
					<h3><i class="fa fa-university" aria-hidden="true"></i>&nbsp E</h3><h4>DUCATIONAL </h4><h3>B</h3><h4>ACKGROUND  </h4>
					<hr>
					<div class="vuni">
						<div class="arrow">				
						2017.09-2018.09
						<span class="jian"></span>
						</div>
						<div class="uni">
							<h4>University of Bristol, Bristol, U.K. </h4>
							<br>#44 QS World University Rankings<sup>®</sup> 2018
							<br>Faculty of Engineering
							<br>MSc, Optical Communications and Signal Processing
						</div>
						<div style="clear:both"></div>
					</div>
					<div class="vuni">
						<div class="arrow">				
						2013.09-2017.07
						<span class="jian"></span>
						</div>
						<div class="uni">
							<h4>Zhejiang University, Hangzhou, CN </h4>
							<br>#84 QS World University Rankings<sup>®</sup> 2018
							<br>College of Optical Science and Engineering
							<br>BEng, Opto-Electronics Information Sci&Eng
						</div>
						<div style="clear:both"></div>
					</div>
				</div>

				<div class="detail">
					<h3><i class="fa fa-suitcase" aria-hidden="true"></i>&nbsp W</h3><h4>ORK </h4><h3>E</h3><h4>XPERIENCE  </h4>
					<hr>
					<div class="vuni">
						<div class="arrow">				
						2017.07-2017.09
						<span class="jian"></span>
						</div>
						<div class="uni">
							<h4>PHP Programmer at STIA, Shanghai </h4>
							<br>Developed the learning management system of
							STIA, a company creating vocational training
							content for the Elderly-Care Sector.
						</div>
						<div style="clear:both"></div>
					</div>
					<div class="vuni">
						<div class="arrow">				
						2015.11-2016.04
						<span class="jian"></span>
						</div>
						<div class="uni">
							<h4>A Customs Declaration Program </h4>
							<br>A platform for customs declaration data analysis
								and processing, developed with PHP and Autoit,
								commissioned by a company in Yiwu, China.
						</div>
						<div style="clear:both"></div>
					</div>
					<div class="vuni">
						<div class="arrow">				
						2015.09-2015.10
						<span class="jian"></span>
						</div>
						<div class="uni">
							<h4>The Official Website of Lily&Beauty Co.,Ltd. </h4>
							<br>An article-based website for displaying corporate
							image, developed with PHP, Javascript, and HTML,
							using the WordPress framework.
						</div>
						<div style="clear:both"></div>
					</div>
				</div>

				<div class="detail">
					<h3><i class="fa fa-code" aria-hidden="true"></i>&nbsp P</h3><h4>ROGRAM </h4><h3>A</h3><h4>BILITY  </h4>
					<hr>
					<div class="vuni">
						<div class="skill">
							<label>MATLAB</label><br>
							<div class="mark">				
							<div style="clear:both"></div>
							<div class="inner" style="width: 20%;"></div>
							</div>
						</div>
						<div class="skill1">
							<label>PHP</label><br>
							<div class="mark">				
							<div style="clear:both"></div>
							<div class="inner" style="width: 15%;"></div>
							</div>
						</div>
						<div class="skill">
							<label>C/C++</label><br>
							<div class="mark">				
							<div style="clear:both"></div>
							<div class="inner" style="width: 12%;"></div>
							</div>
						</div>
						<div class="skill1">
							<label>Javascript</label><br>
							<div class="mark">				
							<div style="clear:both"></div>
							<div class="inner" style="width: 25%;"></div>
							</div>
						</div>
						<div class="skill">
							<label>Linux</label><br>
							<div class="mark">				
							<div style="clear:both"></div>
							<div class="inner" style="width: 30%;"></div>
							</div>
						</div>
						<div class="skill1">
							<label>MySQL</label><br>
							<div class="mark">				
							<div style="clear:both"></div>
							<div class="inner" style="width: 20%;"></div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div style="clear:both"></div>
    </div>
</body>
</html>